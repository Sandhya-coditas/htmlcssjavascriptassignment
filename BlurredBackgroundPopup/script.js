const containerEl = document.querySelector(".container");
const buttonEl = document.querySelector(".button");
const popupContainerEl = document.querySelector(".popupContainer");
const iconEl = document.querySelector(".icon");
buttonEl.addEventListener("click", () => {
  containerEl.classList.add("active");
  popupContainerEl.classList.remove("active");
});
iconEl.addEventListener("click", () => {
  containerEl.classList.remove("active");
  popupContainerEl.classList.add("active");
});

